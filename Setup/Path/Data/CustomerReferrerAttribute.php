<?php

namespace LukaszGrabek\Customer\Setup\Patch\Data;

use LukaszGrabek\Customer\Model\CustomerReferrer\Source;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class CustomerReferrerAttribute
 * @package LukaszGrabek\Customer\Setup\Patch\Data
 */
class CustomerReferrerAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * RegistrationAttributes constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param Config $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->createAttribute('referrer', [
            'label' => 'Referrer',
            'type' => 'varchar',
            'input' => 'select',
            'source' => Source::class,
            'position' => 1300
        ]);
        $this->setAttributeSet('referrer', 1300);
        $this->setUsedInForms('referrer');
    }

    private function createAttribute(string $code, array $config): void
    {
        $attrConfig = array_merge([
            'type' => 'varchar',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'system' => 0,
        ], $config);

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->addAttribute(Customer::ENTITY, $code, $attrConfig);
    }

    private function setUsedInForms(string $code): void
    {
        $attribute = $this->eavConfig->getAttribute(Customer::ENTITY, $code);
        $attribute->setData('used_in_forms', [
            'adminhtml_customer',
            'customer_account_create',
        ]);
        $attribute->save();
    }

    private function setAttributeSet(string $code, int $sortOrder): void
    {
        $eavSetup = $this->eavSetupFactory->create();
        $attrSetId = $eavSetup->getDefaultAttributeSetId(Customer::ENTITY);
        $groupId = $eavSetup->getDefaultAttributeGroupId(Customer::ENTITY, $attrSetId);
        $eavSetup->addAttributeToSet(Customer::ENTITY, $attrSetId, $groupId, $code, $sortOrder);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
