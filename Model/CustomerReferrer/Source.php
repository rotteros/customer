<?php

namespace LukaszGrabek\Customer\Model\CustomerReferrer;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class Source
 */
class Source extends AbstractSource
{

    const VALUE_EMPTY = 0;
    const VALUE_FACEBOOK = 1;
    const VALUE_INSTAGRAM = 2;
    const VALUE_WORD_OF_MOUTH = 3;
    const VALUE_LOCAL_CAFE = 4;
    const VALUE_OTHER = 5;

    /**
     * @return array
     */
    public function getAllOptions()
    {
        return [
            ['label' => 'None', 'value' => self::VALUE_EMPTY],
            ['label' => 'Facebook', 'value' => self::VALUE_FACEBOOK],
            ['label' => 'Instagram', 'value' => self::VALUE_INSTAGRAM],
            ['label' => 'Word of Mouth', 'value' => self::VALUE_WORD_OF_MOUTH],
            ['label' => 'Local Cafe', 'value' => self::VALUE_LOCAL_CAFE],
            ['label' => 'Other', 'value' => self::VALUE_OTHER],
        ];
    }
}
